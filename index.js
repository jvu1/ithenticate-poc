const xmlrpc = require("davexmlrpc");

function log(...args) {
  //console.log(...args);
}

function request(action, params) {
  const urlEndpoint = " https://api.ithenticate.com/rpc";
  const format = "xml";

  return new Promise((resolve, reject) => {
    xmlrpc.client(urlEndpoint, action, [params], format, function (err, data) {
      if (err) {
        const xmlError = new Error(err.message);
        reject(xmlError);
      }
      resolve(data);
    });
  });
}

const localStore = {};

function setState(key, value) {
  localStore[key] = value;
  return localStore;
}

function getState(key) {
  return localStore[key];
}

async function login(username, password) {
  const { sid } = await request("login", {
    username,
    password,
  });

  /**
   * Store to localstorage
   */
  setState("username", username);
  setState("sid", sid);

  log(">> sid", sid);

  return sid;
}

async function getFolder(folderName) {
  const sid = getState("sid");

  /**
   * Find "My Documents" Folder
   */
  const folderListData = await request("folder.list", {
    sid,
  });

  const myFolder = folderListData.folders.find(
    (folder) => folder.name === folderName
  );

  log(">> myFolderFromList", myFolder);

  /**
   * Get detailed folder information
   */
  const folderData = await request("folder.get", {
    sid,
    id: myFolder.id,
  });

  log(">> myFolderDetails", folderData);

  return folderData;
}

async function submitDocument(folderData, title, content) {
  const sid = getState("sid");
  const username = getState("username");
  const folderId = folderData.folder.id;

  /**
   * Upload/Submit document
   */
  const documentData = await request("document.add", {
    sid,
    folder: folderId,
    submit_to: 1,
    uploads: [
      {
        filename: "/tmp/CVxcXdk3lT.txt",
        author_first: username,
        author_last: "",
        upload: Buffer.from(content),
        title,
      },
    ],
  });

  log(">> documentData", documentData);
}

async function pollingReportStatus(
  folderData,
  title,
  pollingIntervalTime,
  timeLeft
) {
  return new Promise(async (resolve, reject) => {
    if (timeLeft <= 0) reject(new Error("Timed out!"));

    console.log(">>>>> Report Pending....");

    const sid = getState("sid");

    const newFolderData = await getFolder(folderData.folder.name);

    log(">>>> docu", newFolderData);

    const document = newFolderData.documents.find((doc) => doc.title === title);

    if (!document) {
      return setTimeout(() => {
        resolve(
          pollingReportStatus(
            newFolderData,
            title,
            pollingIntervalTime,
            timeLeft - pollingIntervalTime
          )
        );
      }, pollingIntervalTime);
    }

    /**
     * Get Document status
     */
    const status = await request("document.get", {
      sid,
      id: document.id,
    });

    log(">> status", status);

    const documentReport = status.documents[0];

    log(">> documentReport", documentReport);

    if (documentReport.is_pending) {
      return setTimeout(() => {
        resolve(
          pollingReportStatus(
            folderData,
            title,
            pollingIntervalTime,
            timeLeft - pollingIntervalTime
          )
        );
      }, pollingIntervalTime);
    }

    log(">> final documentReport", documentReport);
    resolve(documentReport);
  });
}

async function getReport(document) {
  const sid = getState("sid");

  /**
   * Get Similarity report
   */
  const report = await request("report.get_document", {
    sid,
    id: document.id,
  });

  log(">> report", report);

  return report;
}

async function run() {
  console.log(">>>>> Start");
  try {
    /**
     * Login
     */
    await login("jvu@chegg.com", "1Password");

    /**
     * TODO: Upload document
     */

    const folderName = "My Documents";
    const title = "economics/introduction-to-economics/marginal-firm";
    const content = `It has been argued by many neo-classical and neo-Keynesian economists that the money supply has a direct impact on the price level in the economy in the long run. The increase in the money supply tends to have a positive impact on the price level. Not just this, as the change in the price level,occurs;the rate of inflation in an economy also changes. The effect cannot be seen or observed quickly; instead, it takes several financial quarters to show the impact on the basic price level. Hence, any change in the money supply changes the price level in the long run.

    In order to identify and examine the relationship between the money supply and price level, it is important to assess the rate of employment and the level of unemployment in an economy. Because unemployment is the only factor in an economy that affects the money supply indirectly and influences the change in output level, aggregate demand, and finally, the price level. Philips Curve shows the relationship between inflation and unemployment. In the long run, the Philips curve is vertical.

    High Unemployment and the Money Supply
    The high unemployment refers to the lack of the availability of jobs for the workers who are capable and willing to work for wages to sustain their livelihood. Since unemployment is high and companies have laid off workers due to low demand, it is likely to have a negative impact on the efficiency and productivity of the companies which directly hit the total output level of the country. Now, under such a scenario, the government and commercial banks take measures to increase the money supply in the economy. When there is an increase in the money supply, the producers and companies simply increase the output level by employing the unemployed workers at original wages. The increase in the money supply stimulates the demand for a wide variety of goods and services. But, because of the high unemployment that existed, the increase in the money supply does not result in an increase in the price level. Instead, the national output of the country increases and the high unemployment rate decreases in the long run.

    Quantity Theory of Money
    The relationship between the price level and the money supply can also be explained using the Quantity Theory of Money, which states that the general price level of goods and services is directly proportional to the amount of money in circulation in the economy.

    Under both scenarios of high employment and high unemployment, it determines to which extent an increase in the money supply will result in an increase in the price level. But many economists have argued that an increase in the money supply may increase the domestic price level to a larger degree in the long run and lowers the unemployment rate.

    It is important to note that the aggregate supply curve determines the impact of the change in the money supply on total output during the production process. In the long run, if the ASC is vertical, the increase in money supply would result in inflation. And, if the curve is relatively flat, the increase in money supply would increase in the output level and have little impact on the price level.`;

    /**
     * Locate Folder
     */
    const folder = await getFolder(folderName);

    /**
     * Submit document
     */
    await submitDocument(folder, title, content);

    /**
     * Get Report
     */
    const finalDocReport = await pollingReportStatus(
      folder,
      title,
      1000,
      300000
    );

    /**
     * Find appropriate folder to place document
     */
    const url = await getReport(finalDocReport);

    log("url", url);

    console.log(">>>>> URL", url);
    console.log(">>>>> Done!");
  } catch (e) {
    throw e;
  }
}

run();
