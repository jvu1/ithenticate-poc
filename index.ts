import xmlrpc from "xmlrpc";

// Creates an XML-RPC client. Passes the host information on where to
// make the XML-RPC calls.
const client = xmlrpc.createClient({
  host: "api.ithenticate.com",
  path: "/rpc",
});

// Sends a method call to the XML-RPC server
client.methodCall("login", ["jvu@chegg.com", "#S1mple23@"], function (
  error,
  value
) {
  // Results of the method response

  console.error("error", error);

  console.log("Method response for 'login': " + value);
});
